import React from 'react';

import { Grid } from '@material-ui/core';

// import artistDesktop from '../static/images/backgrounds/artist-image.png';
import artistDesktop from '../static/images/backgrounds/artist-image2.jpg';
import artistFallback from '../static/images/backgrounds/artist-image2.jpg';

// import artistMobile from '../static/images/artist.jpg';
import artistMobile from '../static/images/artist-mobile2.jpg';

import ProgressiveImage from 'react-progressive-image';

const Artist = () => {
    return (
        <React.Fragment>
            <div className="artist">
                <div className="artist-banner">
                    <ProgressiveImage src={artistDesktop} placeholder={artistFallback}>
                        {(src,loading) => <img style={{ filter: loading ? 'blur(1px)':'blur(0px)' }} className="banner tablet-only" width="100%" src={src} alt="Artist Jewels by Mala" />}
                    </ProgressiveImage>
                    <img
                        src={artistMobile}
                        className="banner mobile"
                        width="100%"
                        alt="Artist Jewels by Mala"
                    />
                </div>
                <div className="artist-content">
                    <div className="quote-container">
                        <div className="quote">
                            <h2><q>My love for all things beautiful was a defining <br />characteristic from a very early age</q></h2>
                            <h2 className="text-center">- Mala</h2>
                        </div>
                    </div>
                    <Grid
                        className="container"
                        container
                        spacing={2}
                        direction="row"
                        justify="flex-end"
                        alignItems="stretch"
                    >
                        <Grid className="artist-text" item xs={12} md={6}>
                            <p>Innovative, gifted yet unambitious that was me, as a young girl of 15 in Bombay. I found a mentor, guide and philosopher in my grand dad, he pushed me to get into our family business of Jewelry set up by my uncles. It proved to be my first step towards  my lifelong romance with the jewelry. I put my heart and soul to learn the fine art &amp; intricacy of making timeless jewellery. My relentless effort lead me to head 2 stores and 70 teammates. I was learning, excelling, and soon came to be known for my sincerity and good public relationship. Our jewellery was the talk of the town and I was popular in the jewellery circle in Bombay. Soon, 24 years just rolled by, in love, commitment, dedication to learn all about what I Loved- Jewels &amp; Jewellery.</p>
                            <br />
                            <p>The girl who had dreamt of only being a housewife as a young girl, soon had her calling to get married to the man of her dreams and moved to California. I had no plans to setup shop there but as the fate had it, a old client back home came looking for me to help her design jewelry for her daughters wedding, I referred them back home but he insisted that  whenever he thought of Jewellery, I was the first person who came to their mind, moved by the trust and faith bestowed upon me, there was no way I could have said no to him, and that's when it all started once again, only this time it was for ME.</p>
                            <br />
                            <p>With one satisfied customer already in place, many offers soon followed to create contemporary, customized jewellery, that was my rebirth, I christened: Jewels by Mala.inc. in 2015 and embarked on my journey only this time on my own into the magical &amp; bedazzled world of Jewellery. Many designs, innovations, craftsmanship later I still find myself recreating a part of me with my art &amp; finesse to design each  piece of Jewellery which will stay with you forever!</p>
                        </Grid>
                    </Grid>
                    <div className="promise-container">
                        <div className="promise">
                            <h2><q>I make all my jewellery with a sense of commitment &amp; sentiment, hoping it will be treasured with joy. <br />
                                My jewels are a timeless collection, which appeals to all ages, there is a part of jewellery one can own at any age and keep forever.
                            </q></h2>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}
export default Artist;