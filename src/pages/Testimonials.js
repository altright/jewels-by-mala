import React from 'react';
import Slider from "react-slick";

import testimonial from '../static/images/backgrounds/testimonials-background.png';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import leftArrow from '../static/images/icons/left-arrow.png';
import rightArrow from '../static/images/icons/right-arrow.png';

const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight:true,
    prevArrow: <LeftNavButton />,
    nextArrow: <RightNavButton />,


};
function LeftNavButton(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow left-arrow"
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
            <img src={leftArrow} alt="arrow_left" />
        </div>
    );
}
function RightNavButton(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow right-arrow"
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
            <img src={rightArrow} alt="arrow_left" />
        </div>
    );
}
const Testimonials = () => {

    return (
        <div className="testimonial">
            <div className="testimonial-banner">
                <img src={testimonial} className="banner" alt="Jewels by Mala Collection" width='100%' />
            </div>
            <div className="testimonial-content">
                <div className="slider-container">
                    <Slider {...settings}>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}>When I started the process of searching for an engagement ring for my fiancé, it was an easy choice to go to Mala given the track record she had with family and friends of mine.
                            I had never bought jewellery before, let alone a diamond ring, so my level of knowledge was very low.</p><p> She spent months educating me on the different colors, carats, cuts, and so on.
                                She fought relentlessly to find me the best deal possible, and I always felt she was personally invested in making sure I was happy with the price.</p>
                                <p>Once we passed that stage, the design began. Being very indecisive in general and even more given the life long commitment that comes with a ring as it had to be perfect,
                                    Mala was patient with my back and forth on the iterations of the design.</p>
                                <p>On top of being personable, helpful, caring, and great to work with- she brings a wealth of knowledge to ensure you are making the absolute right decision on your purchase.</p>
                                <p> I have referred Mala to countless friends of mine, all of whom are very happy with her just as I am. I say it with full confidence that I will never be purchasing jewellery elsewhere,
                                        and have already made multiple purchases for my upcoming nuptials from her.</p>
                                <p></p>
                                <p style={{ display: 'inline' }}>When one says they have a "family jeweler," that usually means they have a jeweler the entire family goes to. When I introduce Mala as my "family jeweler," I mean she is exactly that,
                                a jeweler who takes care of us as if she is family and always treats us as such.</p></q>
                            <p className="person">-Akshay Oberai</p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}>If you are looking to buy a beautiful piece of jewellery or looking to create one
                                according to your specifications, I would highly recommend Mala.</p><p> She is patient, kind and honest and really works towards making you happy with your purchase.</p>
                                <p>Her experience in the field combined with her knowledge and passion for creating beautiful jewellery
                            will ensure that you get the best pieces at the best value.  </p>
                                <p style={{ display: 'inline' }}>Happy Shopping!</p></q>
                            <p className="person">-Smita Vasant</p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}>At the heart of Jewels by Mala is the talented designer Mala. She creates the most beautiful jewellery, each piece is hand designed and the quality and finish are outstanding. </p>
                                <p>I have purchased a pair of ruby and uncut diamond earrings that I Love! I get so many compliments when I wear them.</p>
                                <p>Thank you Mala for always paying attention to your customers and offering them so many choices. </p>
                                <p style={{ display: 'inline' }}>Love your personal touch.</p></q>
                            <p className="person">-Mina Patel</p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}>Working with Mala was such a pleasure.  She’s truly an amazing jewellery designer who understood exactly how to make my piece perfect.</p>
                                <p>It was exactly what I wanted and I would absolutely work again with her on my future purchases.</p>
                                <p style={{ display: 'inline' }}> She’s kind, understands what the client wants and is easy to work with.
                             </p></q>
                            <p className="person">-Champa Pathiratnel</p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}> I would love to express my gratitude to Mala for a beautiful gift that I received from my husband, designed by her. Without her creativity and excellent taste that would never happen.</p>
                                <p> I love my precious ring and couldn’t be more happy with Mala’s work. She is very dedicated and professional, she takes her job seriously and make you feel like a princess!!!</p>
                                <p>She is not only making woman feeling special but she cares about her customers and she is there for us whenever we need her! If you didn’t have opportunity to work with her, this is the time!</p>
                                <p style={{ display: 'inline' }}>You won’t be disappointed! Very high quality with affordable prices!!! She is incredible! Million thanks Mala!</p></q>
                            <p className="person">-Adriana Radzki</p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}> Since the time I met this sweet charming girl, she has been my jewellery designer.
                        Mala has the  tact of understanding what you desire and her passion for jewellery creates magical pieces.</p>
                                <p style={{ display: 'inline' }}>I have been very satisfied with each of my order with Jewels by Mala, Inc. Wishing her a bright future and success always. </p></q>
                            <p className="person">-Jasmine Menghani </p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}>Mala custom made my sister in law and best friend's beautiful engagement rings.
                        She has a white glove service with her customers, gives the best options, and is constantly asking and understanding the customer's needs.</p>
                                <p style={{ display: 'inline' }}>She is transparent with pricing and is always taking her customer's best interest in hand.
                                I will continue to come to Mala for anything that has to do with high end jewellery. </p></q>
                            <p className="person">-Amina Moinuddin </p>
                        </div>
                        <div className="slider-text-container">
                            <q><p style={{ display: 'inline' }}>I was introduced to Mala by my friend Shilpi .It was that time that Mala was moving to the US to marry my friends Shilpi’s brother .</p>
                                <p>Having an amazing family jewellery business in India she gave it all up for the love she had for her husband and made the US her home
Starting with a brand new venture I was honored to be invited to her first hosting at Shilpi’s home.</p>
                                <p>Very kind and very patient she explained to me her different beautiful pieces that she had designed on her very own. </p>
                                <p>Looking for a pair of earrings for my birthday I was honored to be her first client and I bought a beautiful polki, pearl and diamond earrings. </p>
                                <p style={{ display: 'inline' }}>I still wear it and I treasure this piece so much and so thankful to have got to now make her my friend as well.</p></q>
                            <p className="person">-Gwen Bartlett </p>
                        </div>
                    </Slider>
                </div>
            </div>
        </div>
    );
}
export default Testimonials;