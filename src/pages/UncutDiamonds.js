import React from 'react';
import {NavLink} from 'react-router-dom';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import useWindowDimensions from '../components/windowDimension';
import ProgressiveImage from 'react-progressive-image';

// Images
import leftArrow from '../static/images/icons/left-arrow.png';
import rightArrow from '../static/images/icons/right-arrow.png';

import bangles from '../static/images/backgrounds/uncut-bangles-background.jpg';
import banglesFallback from '../static/images/backgrounds/uncut-bangles-fallback.jpg';

import banglesMobile from '../static/images/bangle-mobile-uncut.jpg';

// import bangle1 from 'static/images/diamond/d-bracelet-1.jpg';
// import bangle1Fallback from 'static/images/diamond/d-bracelet-1-fallback.jpg';

// import bangle2 from 'static/images/diamond/d-bracelet-2.jpg';
// import bangle2Fallback from 'static/images/diamond/d-bracelet-2-fallback.jpg';

// import bangle3 from 'static/images/diamond/d-bracelet-3.jpg';
// import bangle3Fallback from 'static/images/diamond/d-bracelet-3-fallback.jpg';
import fallback from '../static/images/nav-logo.png';
import bangle1 from '../static/images/diamond/ud-bracelet-1.jpg';
import bangle2 from '../static/images/diamond/ud-bracelet-2.JPG';
import bangle3 from '../static/images/diamond/ud-bracelet-3.JPG';
import bangle4 from '../static/images/diamond/ud-bracelet-4.JPG';
import bangle5 from '../static/images/diamond/ud-bracelet-5.png';
import bangle6 from '../static/images/diamond/ud-bracelet-6.png';


import earings from '../static/images/backgrounds/uncut-earings-background.jpg';
import earingsFallback from '../static/images/backgrounds/uncut-earings-fallback.jpg';

import earingsMobile from '../static/images/earing-mobile-uncut.jpg';

import earings1 from '../static/images/diamond/ud-earring-1.jpg';
import earings2 from '../static/images/diamond/ud-earring-2.JPG';
import earings3 from '../static/images/diamond/ud-earring-3.JPG';
// import earings4 from 'static/images/diamond/ud-earring-4.JPG';
import earings4 from '../static/images/diamond/ud-earring-5.JPG';
// import earings5 from 'static/images/diamond/ud-earring-6.JPG';
// import earings7 from 'static/images/diamond/ud-earring-7.jpg';
import earings5 from '../static/images/diamond/ud-earring-8.JPG';


import necklaces from '../static/images/backgrounds/uncut-necklaces-background.jpg';
import necklacesFallback from '../static/images/backgrounds/uncut-necklaces-fallback.jpg';

import necklacesMobile from '../static/images/necklace-mobile-uncut.jpg';

import necklace1 from '../static/images/diamond/ud-necklace-1.JPG';
import necklace2 from '../static/images/diamond/ud-necklace-2.jpg';
import necklace3 from '../static/images/diamond/ud-nnecklace-3.jpg';
import necklace4 from '../static/images/diamond/ud-necklace-4.jpg';
import necklace5 from '../static/images/diamond/ud-necklace-5.JPG';

import pendant from '../static/images/backgrounds/uncut-pendant-background.jpg';
import pendantMobile from '../static/images/backgrounds/uncut-pendant-background.jpg';

import pendantFallback from '../static/images/backgrounds/uncut-pendant-fallback.jpg';

import pendant1 from '../static/images/diamond/ud-pendant-2.JPG';
import pendant2 from '../static/images/diamond/ud-pendant-3.JPG';
import pendant3 from '../static/images/diamond/ud-pendant-1.JPG';
import pendant4 from '../static/images/diamond/ud-pendant-4.JPG';
import pendant5 from '../static/images/diamond/ud-pendant-5.JPG';
import pendant6 from '../static/images/diamond/ud-pendant-6.JPG';
import pendant7 from '../static/images/diamond/ud-pendant-7.JPG';
import pendant8 from '../static/images/diamond/ud-pendant-8.JPG';
import pendant9 from '../static/images/diamond/ud-pendant-9.JPG';
import pendant10 from '../static/images/diamond/ud-pendant-10.JPG';



const diamondsList = [
    {
        "title": "Necklaces",
        "img": necklaces,
        "imgFallback": necklacesFallback,
        "imgMobile": necklacesMobile,
        "styles": "banner-content right",
        "carousel": [

            {
                "img": necklace1,
                "imgTitle": 'Necklace Earings',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": necklace2,

                "imgTitle": 'Necklace Earings 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": necklace3,

                "imgTitle": 'Necklace Earings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace4,

                "imgTitle": 'Necklace Earings 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace5,

                "imgTitle": 'Necklace Earings 4',
                "imgDesc": '29.22 CTS',
            },
        ],

    },
    {
        "title": "Bangles",
        "img": bangles,
        "imgFallback": banglesFallback,
        "imgMobile": banglesMobile,
        "styles": "banner-content right",
        "carousel": [
            {
                "img": bangle1,
                "imgTitle": 'Diamond Bracelet',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": bangle2,
                "imgTitle": 'Diamond Bracelet 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": bangle3,
                "imgTitle": 'Diamond Bracelet 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": bangle4,
                "imgTitle": 'Diamond Bracelet 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": bangle5,
                "imgTitle": 'Diamond Bracelet 5',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": bangle6,
                "imgTitle": 'Diamond Bracelet 6',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Earrings",
        "img": earings,
        "imgFallback": earingsFallback,
        "imgMobile": earingsMobile,
        "styles": "banner-content left",
        "carousel": [
            {
                "img": earings1,

                "imgTitle": 'Diamond Earings',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": earings2,

                "imgTitle": 'Diamond Earings 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": earings3,

                "imgTitle": 'Diamond Earings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": earings4,

                "imgTitle": 'Diamond Earings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": earings5,

                "imgTitle": 'Diamond Earings 3',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Pendant Set",
        "img": pendant,
        "imgFallback": pendantFallback,
        "imgMobile": pendantMobile,
        "styles": "banner-content right",
        "carousel": [
            {
                "img": pendant1,

                "imgTitle": 'Diamond Pendant',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": pendant2,

                "imgTitle": 'Diamond Pendant 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": pendant3,

                "imgTitle": 'Diamond Pendant 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant4,

                "imgTitle": 'Diamond Pendant 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant5,

                "imgTitle": 'Diamond Pendant 5',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant6,

                "imgTitle": 'Diamond Pendant 6',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant7,

                "imgTitle": 'Diamond Pendant 7',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant8,

                "imgTitle": 'Diamond Pendant 8',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant9,

                "imgTitle": 'Diamond Pendant 9',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": pendant10,

                "imgTitle": 'Diamond Pendant 10',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
]

function LeftNavButton(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow slick-left-arrow"
            style={{ ...style, display: 'flex' }}
            onClick={onClick}
        >
            Prev
        </div>
    );
}
function RightNavButton(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow slick-right-arrow"
            style={{ ...style, display: 'flex' }}
            onClick={onClick}
        >
            Next
        </div>
    );
}
function LeftCarousel(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow left-arrow"
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
            <img
                src={leftArrow}
                alt="arrow_left"
            />
        </div>
    );
}
function RightCarousel(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow right-arrow"
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
            <img
                src={rightArrow}
                alt="arrow_right"
            />
        </div>
    );
}
const UncutDiamonds = () => {
    const { width } = useWindowDimensions();
    const [slide, setSlide] = React.useState(0);

    const settings = {
        dots: false,
        infinite: true,
        lazyLoad: true,
        speed: 500,
        fade: true,
        draggable: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: <LeftNavButton />,
        nextArrow: <RightNavButton />,

        afterChange: current => setSlide(current)

    };
    const smallSettings = {
        dots: false,
        infinite: true,
        lazyLoad: true,
        speed: 500,
        fade: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: <LeftCarousel />,
        nextArrow: <RightCarousel />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <div className="uncut-diamonds">
            <NavLink exact to="/diamonds">
                <p className="collection-outlink">Diamonds</p>
            </NavLink>
            <Slider className="slick-slider-mobile" {...settings}>
                {diamondsList.map((diamond) => {
                    return (
                        <div key={diamond.title}>
                            <div className="large-slider-container">
                                <ProgressiveImage
                                    src={diamond.img}
                                    placeholder={diamond.imgFallback}>
                                    {(src, loading) =>
                                        <img style={{
                                            filter: loading ? 'blur(2px)' : 'blur(0px)'
                                        }}
                                            className="banner tablet-only"
                                            width={width}
                                            height="100%"
                                            src={src}
                                            alt={diamond.title} />}
                                </ProgressiveImage>
                                <img
                                    className="banner mobile"
                                    src={diamond.imgMobile}
                                    alt={diamond.title}
                                    width={width}
                                    height="100%"
                                />
                                <div className={`${diamond.styles}`}>
                                    <h2>{diamond.title}</h2>
                                </div>
                            </div>
                        </div>

                    )
                })

                }
            </Slider>

            <div className="small-slider-container">
                <Slider {...smallSettings}>
                    {diamondsList[slide].carousel.map((item) => {
                        return (
                            <div key={item.imgTitle} className="small-slider">
                                <ProgressiveImage
                                    src={item.img}
                                    placeholder={fallback}>
                                    {(src, loading) =>
                                        <img style={{
                                            filter: loading ? 'blur(2px)' : 'blur(0px)'
                                        }}
                                            width={width}
                                            height="100%"
                                            src={src}
                                            alt={item.imgTitle} />}
                                </ProgressiveImage>
                                <div className="small-slider-content">
                                    {/* <h2>{item.imgTitle}</h2> */}
                                    {/* <p>{item.imgDesc}</p> */}
                                </div>
                            </div>
                        )
                    })}
                </Slider>
            </div>
        </div>
    );
}
export default UncutDiamonds;