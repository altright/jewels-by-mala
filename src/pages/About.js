import React from 'react'; 
import about from '../static/images/backgrounds/about-background.png';
import aboutMobile from '../static/images/about.jpg';
import useWindowDimensions from '../components/windowDimension';

export default function About() {
    const { height } = useWindowDimensions();

    return (
        <React.Fragment>
            <div className="about">
                <div className="about-banner">
                <img
                    src={aboutMobile}
                    width="100%"
                    alt="Jewels by Mala Necklace"
                    className="banner mobile"
                />
                </div>
                <img
                    src={about}
                    width="100%"
                    alt="Jewels by Mala Necklace"
                    className="banner tablet-only"
                    height={height}
                />
                <div className="container about-text-container">
                <div className="overlay">
                        </div>
                    <div className="about-grid">
                        

                        <div className="about-text">
                            <h2>At 'Jewels by Mala'</h2>
                            <p>we pride ourselves on our carefully curated assortment of inspired pieces. With unparalleled one-on-one customer service, we are always there to be of assistance and to make you feel secure.</p>
                        <br/>
                        <p>Our designs are influenced with contemporary femininity and these lifelong keepsakes have effortlessly made countless couples happy.
                        </p>
                        <br/>
                        <p>We are devoted to creating original fine jewelry with our heart and soul and are renowned for our stunning custom engagement rings. We are redefining luxury and celebrating uniqueness with our vibrant vision. </p>
                        <br/>
                        <p>Each handmade piece is held to our exceptionally high standards of creativity, quality and craftsmanship. Mala Sujan Seth's luxury lifestyle jewelry is designed to be adorned elegantly and is a reflection of your personality.</p>
                        <br/>
                        <p>Our jewelry designs allow each person to build their own personal collection of diamonds and uncut diamonds that reflects their individual style. Our jewelry is beautiful and striking: a modern take on classic designs.</p>
                        <br/>
                        <p>Mala puts her heart &amp; soul into the collections she creates. We truly believe elegance, luxury and good taste never go out of taste.
                        </p>
                        <br/>
                        <p>Own a piece of 'Jewels by Mala.' It will stay with you forever!</p>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    ); 
}