import React from 'react';
import { Grid } from '@material-ui/core';

import article1 from 'static/images/article1.jpeg';
import article2 from 'static/images/article2.PNG';

const Press = () => {
    return (
        <div className="press">
            <Grid className="press-container" container spacing={4}>
                <Grid item xs={12} md={6}>
                <h2 className="diamond-title">Jewels by Mala Opening</h2>
                    <img className="article1" src={article1} alt="Jewels by Mala Article" />
                </Grid>
                <Grid item xs={12} md={6}>
                <h2 className="diamond-title">Modern Bride Wedding Show in Beverly Hills</h2>
                <a
                    href="https://www.indiawest.com/weddings/wedding-store-hosts-classy-wedding-show-for-the-modern-bride/article_3c3a8bf2-edce-11e8-8f27-172e685f1260.html"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                <img className="article2" src={article2} alt="Jewels by Mala Article" />
                </a>
                </Grid>
            </Grid>
        </div>
    );
}
export default Press;