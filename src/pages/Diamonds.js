import React from 'react';
import {NavLink} from 'react-router-dom';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import useWindowDimensions from '../components/windowDimension';

import ProgressiveImage from 'react-progressive-image';

// Images
import leftArrow from '../static/images/icons/left-arrow.png';
import rightArrow from '../static/images/icons/right-arrow.png';

import fallback from '../static/images/nav-logo.png';

import necklace from '../static/images/backgrounds/diamond-necklaces-background.png';
import necklaceFallback from '../static/images/backgrounds/diamond-necklaces-fallback.jpg';

import necklaceMobile from '../static/images/necklace-mobile-diamond.jpg';

import necklace1 from '../static/images/diamond/d-necklace-1.jpg';

import necklace2 from '../static/images/diamond/d-necklace-2.jpg';

import necklace3 from '../static/images/diamond/d-necklace-3.jpg';

import necklace4 from '../static/images/diamond/d-necklace-4.JPG';
import necklace5 from '../static/images/diamond/d-necklace-5.JPG';
import necklace6 from '../static/images/diamond/d-nnecklace-6.jpg';
import necklace7 from '../static/images/diamond/d-necklace-7.JPG';
import necklace8 from '../static/images/diamond/d-necklace-8.jpg';
import necklace9 from '../static/images/diamond/d-necklace-9.jpg';



import earings from '../static/images/backgrounds/diamond-earings-background.jpg';
import earingsFallback from '../static/images/backgrounds/diamond-earings-fallback.jpg';

import earingsMobile from '../static/images/earing-mobile-diamond.jpg';

import earings1 from '../static/images/diamond/dn-earring-1.jpg';

import earings2 from '../static/images/diamond/dn-earring-2.jpg';

import earings3 from '../static/images/diamond/dn-earring-3.jpg';

import earings4 from '../static/images/diamond/dn-earring-4.jpg';
import earings5 from '../static/images/diamond/dn-earring-5.jpg';
import earings6 from '../static/images/diamond/dn-earring-6.jpg';
import earings7 from '../static/images/diamond/d-earring-7.JPG';
import earings8 from '../static/images/diamond/d-earring-8.JPG';
import earings9 from '../static/images/diamond/d-earring-9.JPG';
import earings10 from '../static/images/diamond/d-earring-10.JPG';
import earings11 from '../static/images/diamond/d-earring-11.JPG';



import rings from '../static/images/backgrounds/diamond-rings-background.jpg';
import ringsFallback from '../static/images/backgrounds/diamond-rings-fallback.jpg';

import ringsMobile from '../static/images/ring-mobile-diamond.jpg';

import rings1 from '../static/images/diamond/d-ring-1.jpg';

import rings2 from '../static/images/diamond/d-ring-2.jpg';

import rings3 from '../static/images/diamond/d-ring-3.jpg';

import rings4 from '../static/images/diamond/d-ring-4.JPG';
import rings5 from '../static/images/diamond/d-ring-5.JPG';

import crings1 from '../static/images/diamond/d-cring-1.jpg';

import crings2 from '../static/images/diamond/d-cring-2.jpg';

import crings3 from '../static/images/diamond/d-cring-3.jpg';

import crings4 from '../static/images/diamond/d-cring-4.jpg';
import crings5 from '../static/images/diamond/d-cring-5.jpg';

import erings1 from '../static/images/diamond/d-ering-1.jpg';

import erings2 from '../static/images/diamond/d-ering-2.jpg';

import erings3 from '../static/images/diamond/d-ering-3.jpg';

import erings4 from '../static/images/diamond/d-ering-4.jpg';

import bracelet from '../static/images/backgrounds/diamond-bracelets-background.jpg';
import braceletFallback from '../static/images/backgrounds/diamond-bracelets-fallback.jpg';

import braceletMobile from '../static/images/bangle-mobile-diamond.jpg';

import bracelet1 from '../static/images/diamond/d-bracelet-1.jpg';

import bracelet2 from '../static/images/diamond/d-bracelet-2.jpg';

import bracelet3 from '../static/images/diamond/d-bracelet-3.jpg';

import bracelet4 from '../static/images/diamond/d-bracelet-4.jpg';
import bracelet5 from '../static/images/diamond/d-bracelet-5.jpg';
import bracelet6 from '../static/images/diamond/d-bracelet-6.jpg';

import ebracelet1 from '../static/images/diamond/d-ebracelet-1.jpg';

import ebracelet2 from '../static/images/diamond/d-ebracelet-2.jpg';


import pendants from '../static/images/backgrounds/diamond-pendants-background.jpg';
import pendantsFallback from '../static/images/backgrounds/diamond-pendants-fallback.jpg';

import pendantsMobile from '../static/images/pendant-mobile-diamond.jpg';

import pendants1 from '../static/images/diamond/d-pendant-1.jpg';

import pendants2 from '../static/images/diamond/d-pendant-2.jpg';

import pendants3 from '../static/images/diamond/d-pendant-3.jpg';

import weddingBand from '../static/images/backgrounds/diamond-wedding-background.jpg';
import wedding1 from '../static/images/diamond/d-wedding-1.JPG';
import wedding2 from '../static/images/diamond/d-wedding-2.JPG';
import wedding3 from '../static/images/diamond/d-wedding-3.JPG';
import wedding4 from '../static/images/diamond/d-wedding-4.JPG';


const diamondsList = [
    {
        "title": "Necklaces",
        "img": necklace,
        "imgFallback": necklaceFallback,

        "imgMobile": necklaceMobile,

        "styles": "banner-content right",
        "carousel": [
            {
                "img": necklace1,

                "imgTitle": 'Necklace Earings',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": necklace2,

                "imgTitle": 'Necklace Earings 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": necklace3,

                "imgTitle": 'Necklace Earings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace4,
                "imgTitle": 'Necklace Earings 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace5,
                "imgTitle": 'Necklace Earings 5',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace6,
                "imgTitle": 'Necklace Earings 6',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace7,
                "imgTitle": 'Necklace Earings 7',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace8,
                "imgTitle": 'Necklace Earings 7',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": necklace9,
                "imgTitle": 'Necklace Earings 7',
                "imgDesc": '29.22 CTS',
            },

        ],

    },
    {
        "title": "Earrings",
        "img": earings,
        "imgFallback": earingsFallback,
        "imgMobile": earingsMobile,

        "styles": "banner-content left",
        "carousel": [
            {
                "img": earings1,

                "imgTitle": 'Diamond Earings',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": earings2,

                "imgTitle": 'Diamond Earings 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": earings3,

                "imgTitle": 'Diamond Earings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": earings4,
                "imgTitle": 'Diamond Earings 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": earings5,
                "imgTitle": 'Diamond Earings 5',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": earings6,
                "imgTitle": 'Diamond Earings 5',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Engagement Rings",
        "img": rings,
        "imgFallback": ringsFallback,

        "imgMobile": ringsMobile,

        "styles": "banner-content right",
        "carousel": [
            {
                "img": erings1,

                "imgTitle": 'Diamond Rings',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": erings2,

                "imgTitle": 'Diamond Rings 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": erings3,

                "imgTitle": 'Diamond Rings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": erings4,

                "imgTitle": 'Diamond Rings 4',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Cocktail Rings",
        "img": rings,
        "imgFallback": ringsFallback,

        "imgMobile": ringsMobile,

        "styles": "banner-content right",
        "carousel": [
            {
                "img": crings1,

                "imgTitle": 'Diamond Rings',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": crings2,

                "imgTitle": 'Diamond Rings 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": crings3,

                "imgTitle": 'Diamond Rings 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": crings4,

                "imgTitle": 'Diamond Rings 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": crings5,

                "imgTitle": 'Diamond Rings 5',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Bangles & Bracelet",
        "img": bracelet,
        "imgFallback": braceletFallback,
        "imgMobile": braceletMobile,
        "styles": "banner-content right",
        "carousel": [
            {
                "img": bracelet1,

                "imgTitle": 'Diamond Bracelet',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": bracelet2,

                "imgTitle": 'Diamond Bracelet 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": bracelet3,

                "imgTitle": 'Diamond Bracelet 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": bracelet4,

                "imgTitle": 'Diamond Bracelet 4',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": bracelet5,

                "imgTitle": 'Diamond Bracelet 5',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": bracelet6,

                "imgTitle": 'Diamond Bracelet 6',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Eternity Bands",
        "img": bracelet,
        "imgFallback": braceletFallback,
        "imgMobile": braceletMobile,
        "styles": "banner-content right",
        "carousel": [
            {
                "img": ebracelet1,

                "imgTitle": 'Diamond Bracelet',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": ebracelet2,

                "imgTitle": 'Diamond Bracelet 2',
                "imgDesc": '29.21 CTS',
            }
        ],
    },
    {
        "title": "Pendants",
        "img": pendants,
        "imgFallback": pendantsFallback,

        "imgMobile": pendantsMobile,
        "styles": "banner-content left",
        "carousel": [
            {
                "img": pendants1,

                "imgTitle": 'Diamond Pendant',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": pendants2,

                "imgTitle": 'Diamond Pendant 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": pendants3,

                "imgTitle": 'Diamond Pendant 3',
                "imgDesc": '29.22 CTS',
            },
        ],
    },
    {
        "title": "Wedding Band",
        "img": weddingBand,
        "imgFallback": weddingBand,

        "imgMobile": weddingBand,
        "styles": "banner-content right",
        "carousel": [
            {
                "img": wedding1,

                "imgTitle": 'Diamond Wedding Band',
                "imgDesc": '29.20 CTS',
            },
            {
                "img": wedding2,

                "imgTitle": 'Diamond Wedding Band 2',
                "imgDesc": '29.21 CTS',
            },
            {
                "img": wedding3,

                "imgTitle": 'Diamond Wedding Band 3',
                "imgDesc": '29.22 CTS',
            },
            {
                "img": wedding4,

                "imgTitle": 'Diamond Wedding Band 4',
                "imgDesc": '29.22 CTS',
            },
        ],
    }
]

function LeftNavButton(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow slick-left-arrow"
            style={{ ...style, display: 'flex' }}
            onClick={onClick}
        >
            Prev
        </div>
    );
}
function RightNavButton(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow slick-right-arrow"
            style={{ ...style, display: 'flex' }}
            onClick={onClick}
        >
            Next
        </div>
    );
}
function LeftCarousel(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow left-arrow"
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
            <img
                src={leftArrow}
                alt="arrow_left"
            />
        </div>
    );
}
function RightCarousel(props) {
    const { style, onClick } = props
    return (
        <div
            className="slick-arrow right-arrow"
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
            <img
                src={rightArrow}
                alt="arrow_right"
            />
        </div>
    );
}
const Diamonds = () => {
    const [slide, setSlide] = React.useState(0);

    const { width } = useWindowDimensions();
    const settings = {
        dots: false,
        infinite: true,
        lazyLoad: true,
        speed: 500,
        fade: true,
        draggable: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: <LeftNavButton />,
        nextArrow: <RightNavButton />,

        afterChange: current => setSlide(current)

    };
    const smallSettings = {
        dots: false,
        infinite: true,
        speed: 500,
        lazyLoad: true,
        fade: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: <LeftCarousel />,
        nextArrow: <RightCarousel />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <div className="diamonds">
            <NavLink exact to="/uncut-diamonds">
                <p className="collection-outlink">Uncut Diamonds</p>
            </NavLink>
            <Slider className="slick-slider-mobile" {...settings}>
                {diamondsList.map((diamond) => {
                    return (
                        <div key={diamond.title}>
                            <div className="large-slider-container">
                                <ProgressiveImage
                                    src={diamond.img}
                                    placeholder={diamond.imgFallback}>
                                    {(src, loading) =>
                                        <img style={{
                                            filter: loading ? 'blur(2px)' : 'blur(0px)'
                                        }}
                                            className="banner tablet-only"
                                            width={width}
                                            height="100%"
                                            src={src}
                                            alt={diamond.title} />}
                                </ProgressiveImage>
                                <img
                                    className="banner mobile"
                                    src={diamond.imgMobile}
                                    alt={diamond.title}
                                    width={width}
                                    height="100%"
                                />
                                <div className={`${diamond.styles}`}>
                                    <h2>{diamond.title}</h2>
                                </div>
                            </div>
                        </div>

                    )
                })

                }
            </Slider>

            <div className="small-slider-container">
                <Slider {...smallSettings}>
                    {diamondsList[slide].carousel.map((item) => {
                        return (
                            <div key={item.imgTitle} className="small-slider">
                                <ProgressiveImage
                                    src={item.img}
                                    placeholder={fallback}>
                                    {(src, loading) =>
                                        <img style={{
                                            filter: loading ? 'blur(2px)' : 'blur(0px)'
                                        }}
                                            width={width}
                                            height="100%"
                                            src={src}
                                            alt={item.imgTitle} />}
                                </ProgressiveImage>
                                <div className="small-slider-content">
                                    {/* <h2>{item.imgTitle}</h2>
                                    <p>{item.imgDesc}</p> */}
                                </div>
                            </div>
                        )
                    })}
                </Slider>
            </div>


        </div>
    );
}
export default Diamonds;