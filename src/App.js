import React from 'react';

import Layout from './components/Layout';
import ScrollToTop from './components/ScrollToTop';

import { Redirect } from 'react-router'

import About from 'pages/About';
import Press from 'pages/Press';
import Collection from 'pages/Collection';
import Artist from 'pages/Artist';
import Testimonials from 'pages/Testimonials';
import Diamonds from 'pages/Diamonds';
import UncutDiamonds from 'pages/UncutDiamonds';
import Contact from 'pages/Contact';
import Home from 'pages/Home'; 

import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';

import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';


function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Route render={({ location }) => (
          <ScrollToTop>
            <TransitionGroup>
              <CSSTransition
                key={location.key}
                timeout={800}
                classNames="fade"
              >
                <Switch location={location}>
                  <Redirect exact from="/" to="/home" />
                  <Route path="/home" exact component={Home} />
                  <Route path="/about" exact component={About} />
                  <Route path="/collection" exact component={Collection} />
                  <Route path="/contact" exact component={Contact} />
                  <Route path="/artist" exact component={Artist} />
                  <Route path="/diamonds" exact component={Diamonds} />
                  <Route path="/press" exact component={Press} />
                  <Route path="/uncut-diamonds" exact component={UncutDiamonds} />
                  <Route path="/testimonials" exact component={Testimonials} />
                </Switch>
              </CSSTransition>
            </TransitionGroup>
          </ScrollToTop>
        )} />
      </Layout>
    </BrowserRouter>
  );
}

export default App;
