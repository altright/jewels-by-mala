import React from 'react';
import {
    List,
    ListItem
} from '@material-ui/core'
import pinterest from '../../static/images/icons/pinterest-icon.png';
import facebook from '../../static/images/icons/facebook-icon.png';
import instagram from '../../static/images/icons/instagram-icon.png';
// import twitter from '../../static/images/icons/twitter-icon.png';
import { NavLink } from 'react-router-dom';
const Footer = (props) => {
    return (
        <div className="footer container">
            <List className="footer-list">
                <ListItem button onClick={props.open}>Home</ListItem>
                <NavLink exact to="/artist">
                    <ListItem button>Artist</ListItem>
                </NavLink>
                <NavLink exact to="/collection">
                    <ListItem button>Collection</ListItem>
                </NavLink>
                <NavLink exact to="/testimonials">
                    <ListItem button>Testimonials</ListItem>
                </NavLink>
                <NavLink exact to="/contact">
                    <ListItem button>Contact</ListItem>
                </NavLink>
                <NavLink exact to="/press">

                    <ListItem button>Press</ListItem>
                </NavLink>
            </List>
            <hr />
            <p className="contact-number">For Appointments: +1 714-815-0820</p>
            <div className="copyright">
                <p>Copyright &copy;JEWELS BY MALA 2019 &nbsp; | &nbsp; Created by Pinxitblue &nbsp;|&nbsp; Privacy policy Term of use Credits</p>
                <div className="social-container">
                    <a href="https://in.pinterest.com/sujanseth/" target="_blank" rel="noopener noreferrer">
                        <img src={pinterest} alt="Pinterest" />
                    </a>
                    <a href="https://www.facebook.com/Jewels-by-Mala-Inc-189831474707740" target="_blank" rel="noopener noreferrer">
                        <img src={facebook} alt="Facebook" />
                    </a>
                    <a href="https://www.instagram.com/jewelsbymala/" target="_blank" rel="noopener noreferrer">
                        <img src={instagram} alt="Instagram" />
                    </a>
                    {/* <img src={twitter} alt="Twitter" /> */}
                </div>
            </div>
        </div>
    );
}
export default Footer;